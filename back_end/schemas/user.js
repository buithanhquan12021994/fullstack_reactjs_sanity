export default {
  name: 'user', // name table
  title: 'User Schema', // text description
  type: 'document',
  fields: [ // fields are column
    {
      name: 'userName',  // name column
      title: 'Username', // title
      type: 'string'  // type
    },
    {
      name: 'image',
      title: 'Image',
      type: 'string'
    },
  ]
}