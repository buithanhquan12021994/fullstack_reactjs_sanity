export default {
  name: 'postedBy',
  title: 'PostedBy schema',
  type: 'reference',
  to: [{ type: 'user' }]
}