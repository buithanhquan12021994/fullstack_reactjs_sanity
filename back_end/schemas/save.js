export default {
  name: 'save',
  title: 'Save Schema',
  type: 'document',
  fields: [
    {
      name: 'postedBy',
      title: 'Posted by',
      type: 'postedBy'
    },
    {
      name: 'userId',
      title: 'User Id',
      type: 'string'
    }
  ]
}