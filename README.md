## BACK END
- using sanity-cli to generate backend source
### 1) command sanity
> sanity docs

> sanity manage

> sanity help

> sanity start


## FRONT END

### Require

- reactjs
- tailwindcss
- https://console.cloud.google.com/ ( Credential google )

### SETUP

> cd front_end

> npx create-react-app ./

> npm install -D tailwindcss postcss autoprefixer

> npx tailwindcss init -p

#### GET PROJECT ID SANITY
> sanity manage 

> get ProjectID & put it to .env file

#### GET TOKEN ID SANITY
> sanity manage 

> go to API -> add API TOKEN -> editor

#### SETUP TO allow localhost front-end to access resource without CORS
> sanity manage 

> go to API -> CORS origin -> add -> type: **http://localhost:3000**


### Create account console.cloud.google.com

1. Access [console.cloud.google.com](https://console.cloud.google.com/)
2. Create a new project
3. Choose CREATE CREDENTIALS -> OAUTH Client ID
4. Fill information -> save and continous
5. Back to dashboard -> CREATE CREDENTIALS -> OAUTH Client ID
6. Application type: Web app
7. Fill information
8. Create a credential
